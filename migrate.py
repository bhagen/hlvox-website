"""Migrate from tinydb to sqlalchemy based hlvox version.
Migration is done at an API level between a service running the older version
and a service running the newer version.
"""

import argparse
import json
import logging
import sys
from urllib.parse import urlencode

import requests

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description="Mirror generated sentences from one Vox API to another")
    parser.add_argument("source", type=str, help="Source API")
    parser.add_argument("dest", type=str, help="Destination API")

    args = parser.parse_args()

    source_api = args.source
    dest_api = args.dest

    source_result = requests.get(f"{source_api}/voices", timeout=2)
    source_result.raise_for_status()
    source_voices = json.loads(source_result.content)["voices"]
    dest_result = requests.get(f"{dest_api}/voices", timeout=2)
    dest_result.raise_for_status()
    dest_voices = json.loads(dest_result.content)["voices"]

    logging.info("Checking that voices match")
    if source_voices != dest_voices:
        logging.error('Source and destination voices differ: %s != %s',
                      source_voices, dest_voices)
        sys.exit(1)

    logging.info("Checking that words for each voice match")
    source_generated = {}
    for voice in source_voices:
        source_result = requests.get(f"{source_api}/voice/{voice}", timeout=2)
        source_result.raise_for_status()
        source_parsed = json.loads(source_result.content)
        source_voice_words = source_parsed["words"]

        dest_result = requests.get(f"{dest_api}/voice/{voice}", timeout=2)
        dest_result.raise_for_status()
        dest_voice_words = json.loads(dest_result.content)['words']

        if source_voice_words != dest_voice_words:
            source_set = set(source_voice_words)
            dest_set = set(dest_voice_words)
            dest_missing = source_set - dest_set
            source_missing = dest_set - source_set
            logging.error('Words for %s do not match. Source is missing %s and dest is missing %s',
                          voice, source_missing, dest_missing)
            sys.exit(1)

        source_generated[voice] = source_parsed["generated"]

    logging.info("Transferring generated sentences")

    for voice in source_voices:
        logging.info("Working on %s", voice)
        sentences = source_generated[voice]
        for sentence in sentences:
            if '_comma' in sentence or '_period' in sentence:
                logging.warning('Skipping %s', sentence)
                continue
            params = {"sentence": sentence}
            URL = f"{dest_api}/voice/{voice}/sentence_audio?{urlencode(params)}"
            result = requests.get(URL, timeout=2)
            result.raise_for_status()

    for voice in source_voices:
        logging.info("Verifying %s", voice)
        source_result = requests.get(f"{source_api}/voice/{voice}", timeout=2)
        source_result.raise_for_status()
        source_voice_sentences = set(json.loads(
            source_result.content)["generated"])

        dest_result = requests.get(f"{dest_api}/voice/{voice}", timeout=2)
        dest_result.raise_for_status()
        dest_voice_sentences = set(json.loads(
            dest_result.content)["generated"])

        logging.info("Sentences for %s missing from remote: %s",
                     voice, source_voice_sentences - dest_voice_sentences)
        logging.info("Sentences for %s missing from source: %s",
                     voice, dest_voice_sentences - source_voice_sentences)

    logging.info("Done!")
    sys.exit(0)
