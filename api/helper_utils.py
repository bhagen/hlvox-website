"""Helper utilities/functions
"""
import json
from pathlib import Path


def process_config(filepath: Path) -> dict:
    """Process config file

    Args:
        filepath (Path): Path to config file

    Returns:
        dict: config file dict
    """
    default = {
        "voice_path": "/app/voices",
        "db_path": "/app/db",
    }
    if not filepath.exists():
        filepath.parent.mkdir(parents=True, exist_ok=True)
        with open(filepath, 'w', encoding='UTF-8') as file:
            json.dump(default, file)
            return default
    else:
        with open(filepath, encoding='UTF-8') as file:
            return json.load(file)
