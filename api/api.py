"""Defines hlvox web API
"""

import io
import logging
import os
import random
import typing
from pathlib import Path
from typing import Optional

import hlvox
import pydantic
from fastapi import FastAPI, HTTPException
from fastapi.responses import Response
from helper_utils import process_config

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

config = process_config(Path("./config/config.json"))

# TODO: pylint is angry about this line for some reason, figure out why
database_info = hlvox.manager.RemoteDatabaseInfo(  # pylint: disable=no-member
    name=os.environ.get("POSTGRES_DB"),
    url=os.environ.get("POSTGRES_URL", "db"),
    port=os.environ.get("POSTGRES_PORT", 5432),
    username=os.environ.get("POSTGRES_USER"),
    password=os.environ.get("POSTGRES_PASSWORD"),
)
vox_man = hlvox.Manager(
    voices_path=config["voice_path"],
    database_info=database_info,
)

app = FastAPI()


class VoicesResponse(pydantic.BaseModel):
    """Response to /voices"""

    voices: list[str]


@app.get("/voices")
def get_voices() -> VoicesResponse:
    """Get available voices
    Returns:
       VoicesResponse
    """
    voices = vox_man.get_voice_names()
    return VoicesResponse(voices=voices)


class GetVoiceResponse(pydantic.BaseModel):
    """Response to /voice"""

    # Words a voice can say
    words: list[str]
    # Categories of words
    categories: dict[str, list[str]]
    # Previously generated sentences
    generated: list[str]

    @classmethod
    def from_voice(cls, voice: hlvox.voice.Voice, with_voice=False) -> "GetVoiceResponse":
        """Populate response from Voice object

        Args:
            voice (hlvox.voice.Voice): Voice object to populate from
        """
        return cls(
            words=[word.as_str(with_voice=with_voice) for word in voice.words],
            categories=voice.categories,
            generated=voice.get_generated_sentences(),
        )


@app.get("/voice/{voice_name}")
def get_voice(voice_name: str) -> GetVoiceResponse:
    """Get information about a voice

    Args:
        voice_name (str): Voice name to get info of

    Returns:
        GetVoiceResponse
    """
    voice = vox_man.get_voice(voice_name)
    if voice is None:
        return "Not found", 404

    # Special case: add voice to word string representation if multi-voice
    with_voice = voice_name == "multi"

    return GetVoiceResponse.from_voice(voice=voice, with_voice=with_voice)


class SentenceInfoResponse(pydantic.BaseModel):
    """Response to /voice/{voice_name}/sentence_info (and other similar)"""

    sentence: str
    sayable: list[str]
    unsayable: list[str]

    @classmethod
    def from_sentence(cls, sentence: hlvox.voice.Sentence, with_voice=False) -> "SentenceInfoResponse":
        """Populate response from Sentence object

        Args:
            sent_info (hlvox.voice.Sentence): Sentence object to populate from
        """
        return cls(
            sentence=sentence.sentence,
            sayable=[word.as_str(with_voice=with_voice) for word in sentence.sayable],
            unsayable=[word.as_str(with_voice=with_voice) for word in sentence.unsayable],
        )


@app.get("/voice/{voice_name}/sentence_info")
def sentence_info(voice_name: str, sentence: str) -> SentenceInfoResponse:
    """Get sentence info

    Args:
        voice_name (str): Voice to say sentence with
        sentence (str): Sentence to say

    Returns:
        SentenceInfoResponse
    """
    voice = vox_man.get_voice(voice_name)
    if voice is None:
        raise HTTPException(status_code=404, detail="Not found")

    # Special case: add voice to word string representation if multi-voice
    with_voice = voice_name == "multi"

    try:
        sentence = voice.generate_audio(sentence, dry_run=True)
        return SentenceInfoResponse.from_sentence(sentence=sentence, with_voice=with_voice)
    except Exception as exc:  # pylint: disable=broad-except
        raise HTTPException(status_code=500, detail=str(exc)) from exc


@app.get(
    "/voice/{voice_name}/sentence_audio",
    responses={200: {"content": {"audio/mp3": {}}}},
    response_class=Response,
)
def sentence_audio(voice_name: str, sentence: str) -> bytes:
    """Get audio of a sentence

    Args:
        voice_name (str): Voice to say sentence with
        sentence (str): Sentence to say

    Returns:
        Union[Tuple[str, int], bytes]: Either an error code, or raw sentence audio
    """
    voice = vox_man.get_voice(voice_name)
    if voice is None:
        raise HTTPException(status_code=404, detail="Not found")

    sent_info = voice.generate_audio(sentence)

    if sent_info.audio is None:
        raise HTTPException(status_code=500, detail="Audio generation error")

    with io.BytesIO() as buf:
        sent_info.audio.export(buf, audio_format="mp3")
        return Response(content=buf.getvalue(), media_type="audio/mp3")


@app.get("/voice/{voice_name}/random_generated")
def get_random_generated(voice_name: Optional[str] = None) -> SentenceInfoResponse:
    """Get a random previously generated sentence

    Args:
        voice_name (Optional[str]): Voice name to get sentence from. If none supplied, random voice is chosen.

    Returns:
        SentenceInfoResponse
    """
    if not voice_name:
        available_voices = vox_man.get_voice_names()
        voice_name = available_voices[random.randint(0, len(available_voices) - 1)]
    voice = vox_man.get_voice(voice_name)
    if voice is None:
        raise HTTPException(status_code=404, detail="Not found")

    # Special case: add voice to word string representation if multi-voice
    with_voice = voice_name == "multi"

    generated = voice.get_generated_sentences()
    if not generated:
        # TODO: Doesn't seem like the right code
        raise HTTPException(status_code=404, detail="No generated sentences available")

    sentence = voice.generate_audio(
        generated[random.randint(0, len(generated) - 1)], dry_run=True
    )
    return SentenceInfoResponse.from_sentence(sentence=sentence, with_voice=with_voice)


@app.get("/voice/{voice_name}/random")
def get_random(
    voice_name: typing.Optional[str], num_words: int
) -> SentenceInfoResponse:
    """Generate a random sentence

    Args:
        voice_name (typing.Optional[str]): Voice name to generate sentence with
        num_words (int): How many words in sentence

    Returns:
        SentenceInfoResponse
    """
    if not voice_name:
        available_voices = vox_man.get_voice_names()
        voice_name = available_voices[random.randint(0, len(available_voices) - 1)]
    voice = vox_man.get_voice(voice_name)

    # Special case: add voice to word string representation if multi-voice
    with_voice = voice_name == "multi"

    available_words = voice.words
    words = [
        available_words[random.randint(0, len(available_words) - 1)].as_str()
        for _ in range(num_words)
    ]

    sentence = voice.generate_audio(" ".join(words), dry_run=True)
    return SentenceInfoResponse.from_sentence(sentence=sentence, with_voice=with_voice)
