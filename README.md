# HLVox Website
This is a Flask/Connexion "wrapper" for the
[hlvox](https://gitlab.com/bhagen/hlvox) package in the form of a Docker
package. Connexion provides an API in the form of REST (but I don't really
know what I'm talking about when I say REST so I'm certainly not going to say
its RESTful). Flask runs a front-end website for sentence generation,viewing
of voice vocabulary, and viewing of previously generated sentences.

[Docker Hub Page](https://hub.docker.com/r/bhagen55/hlvoxwebsite)

## Docker Volumes
### Voice Files Mount:
Voice files are required. These are expected to be in
folders named as the voice. For example:
```
<bind-dir/volume>
    vox1
        hello.wav
        gordon.wav
        freeman.wav
    vox2
        hello.wav
        doctor.wav
        kleiner.wav
```

Mount example: \
**<bind-dir/volume>:/app/voices**

## Env:
A `db.env` file is needed for database configuration. See `db.env.example` for what is required. It should be placed at the root of this repo in the same level as `docker-compose.yml`.

## Running:
```
docker compose up
```
The web interface is accessible at port 80, and the API is hosted on port 8000.
